"use strict";
class Employee{
  constructor(name, age, salary){
  this._name = name;
  this._age = age;
  this._salary = salary; 
  }
  get name() {
    return this._name;
  }
  set name (value) {
    this._name = value;
  }
  get age() {
    return this._age;
  }
  set age(value) {
    this._age = value;
  }
  get salary() {
    return this._salary;
  }
  set salary (value) {
    this._salary = value;
  }
}
      
class Programmer extends Employee {
  constructor(name, age, salary, lang) {
  super(name, age, salary);
  this._lang = lang;
  }
  get lang() {
    return this._lang;
  }
  set lang(value) {
    this._lang = value;
  }
  get salary() {
    return (this._salary*3)
  }
}
let John = new Programmer("John", "28", "12000", "C#");
let Dick = new Programmer("Dick", "36", "25000", "Java");
let Will = new Programmer("Will", "46", "33000", "Perl");

console.log(John, Dick, Will);